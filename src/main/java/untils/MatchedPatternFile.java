package untils;

public class MatchedPatternFile {
	
	private String filename;
	private long occurance;
	
	public MatchedPatternFile(String filename, long occurance) {
		this.filename = filename;
		this.occurance = occurance;
	}

	public String getFilename() {
		return filename;
	}

	public long getOccurance() {
		return occurance;
	}

}
