package interview;

import java.io.File;
import java.util.List;

import finders.FileFinder;
import finders.PatternFinder;

public class Main {


	
	/**
	 * @param args
	 * First parameter-starting directory path
	 * Second parameter-file extension 
	 * Third parameter-pattern
	 * 
	 * Example: C:\Program Files txt mama
	 * 
	 */
	public static void main(String[] args) {

		FileFinder finder=new FileFinder(args[0],args[1]);
		List<File>files=finder.findFiles();
		
		PatternFinder patternFinder=new PatternFinder(args[2]);
		patternFinder.logResult(files);

	}
		

}
