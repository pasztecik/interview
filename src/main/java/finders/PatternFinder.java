package finders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import untils.MatchedPatternFile;
import untils.OutputLogger;

public class PatternFinder {

	private final String PATTERN;
	private long sumOfOccurances = 0;
	private List<MatchedPatternFile> foundFiles = new ArrayList<>();

	public PatternFinder(String pattern) {

		this.PATTERN = pattern;

	}

	public void logResult(List<File> filesToSearch) {

		findPattern(filesToSearch);
		OutputLogger.logOutput(foundFiles, PATTERN, sumOfOccurances);
	}

	private void findPattern(List<File> filesToSearch) {

		foundFiles = new ArrayList<>();
		for (File file : filesToSearch) {

			long patternOccuranceInFile = 0;
			try (FileInputStream stream = new FileInputStream(file.getAbsolutePath());
					Scanner scanner = new Scanner(stream)) {
				while (scanner.hasNextLine()) {
					final String line = scanner.nextLine();
					long words = Arrays.stream(line.split(" ")).filter(word -> word.equals(PATTERN)).count();

					patternOccuranceInFile += words;

				}
				foundFiles.add(new MatchedPatternFile(file.getName(), patternOccuranceInFile));
				sumOfOccurances += patternOccuranceInFile;

			} catch (FileNotFoundException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}

	}

}
