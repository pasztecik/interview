package finders;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class FileFinder {

	private final String PATH;
	private final String[] EXTENSION;

	private List<File> filesWithGivenExtension;

	public FileFinder(String path, String extension) {

		this.PATH = path;
		this.EXTENSION = new String[] { extension };
		this.filesWithGivenExtension = null;

	}

	public List<File> findFiles() {

		if (filesWithGivenExtension == null) {

			File file = new File(PATH);
			filesWithGivenExtension = (List<File>) FileUtils.listFiles(file, EXTENSION, true);

		}

		return filesWithGivenExtension;

	}

}
